import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/Model/todo_model.dart';
import 'package:todo_app/Provider/todo_list.dart';
import 'package:todo_app/Screen/Todo/Controller/todo_controller.dart';
import 'package:todo_app/Util/constants.dart';
import 'package:todo_app/Util/util.dart';

class Todo extends StatelessWidget {
  addTodo(BuildContext context) async {
    String content = '';
    String dueDate = '';
    await showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        isDismissible: false,
        isScrollControlled: true,
        builder: (context) => StatefulBuilder(
              builder: (BuildContext context, setState) {
                return Container(
                  width: double.infinity,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TextButton(
                              onPressed: () => Navigator.pop(context),
                              style: ButtonStyle(foregroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(69, 76, 96, 1))),
                              child: Text(
                                'Cancel',
                                style: TextStyle(fontSize: SizeUtil().calculateHeightPx(context, 16)),
                              )),
                          TextButton(
                              onPressed: () async {
                                if (content.isNotEmpty && dueDate.isNotEmpty) {
                                  TodoController().addTodoContent(context, content, dueDate);
                                  Navigator.pop(context);
                                } else {
                                  await showDialog(
                                      context: context,
                                      builder: (context) => Platform.isAndroid
                                          ? AlertDialog(
                                              title: Text('Please complete the information'),
                                              content: Text('${content.isEmpty ? 'Task name is required' : 'Due Date is required'}'),
                                              actions: [TextButton(onPressed: () => Navigator.pop(context), child: Text('OK'))],
                                            )
                                          : CupertinoAlertDialog(
                                              title: Text('Please complete the information'),
                                              content: Text('${content.isEmpty ? 'Task name is required' : 'Due Date is required'}'),
                                              actions: [TextButton(onPressed: () => Navigator.pop(context), child: Text('OK'))],
                                            ));
                                }
                              },
                              style: ButtonStyle(foregroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(69, 76, 96, 1))),
                              child: Text(
                                'Save',
                                style: TextStyle(fontSize: SizeUtil().calculateHeightPx(context, 16)),
                              )),
                        ],
                      ),
                      Divider(),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: SizeUtil().calculateWidthPx(context, 24)),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextField(
                              autofocus: true,
                              onChanged: (text) {
                                setState(() {
                                  content = text;
                                });
                              },
                              decoration: InputDecoration(
                                  hintText: 'Task name...', hintStyle: TextStyle(color: Color.fromRGBO(167, 178, 172, 1)), border: InputBorder.none),
                            ),
                            SizedBox(height: SizeUtil().calculateHeightPx(context, 24)),
                            Text(
                              'Due Date *',
                              style: TextStyle(fontSize: SizeUtil().calculateHeightPx(context, 16)),
                            ),
                            SizedBox(height: SizeUtil().calculateHeightPx(context, 8)),
                            GestureDetector(
                              onTap: () async {
                                FocusScope.of(context).requestFocus(new FocusNode());
                                dueDate = (await showDatePicker(
                                            context: context, initialDate: DateTime.now(), firstDate: DateTime.now(), lastDate: DateTime(2050)))
                                        ?.toIso8601String() ??
                                    '';
                              },
                              child: Container(
                                height: 48.0,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Color.fromRGBO(218, 229, 223, 1)),
                                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text([null, ''].contains(dueDate) ? 'วัน / เดือน / ปี พ.ศ.' : DateUtil().convertToBE(dueDate),
                                          style: TextStyle(color: [null, ''].contains(dueDate) ? greyColor : defaultTextColor)),
                                      Image.asset('Assets/CalendarGreenIcon.png'),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: SizeUtil().calculateHeightPx(context, 32)),
                            SizedBox(height: MediaQuery.of(context).viewInsets.bottom)
                          ],
                        ),
                      )
                    ],
                  ),
                );
              },
            ));
  }

  confirmExit(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (context) => Platform.isAndroid
            ? AlertDialog(
                title: Text('Do you want to exit application?'),
                actions: [
                  TextButton(
                      onPressed: () => Navigator.pop(context, false),
                      child: Text(
                        'NO',
                      )),
                  TextButton(
                      onPressed: () => Navigator.pop(context, true),
                      child: Text(
                        'YES',
                      )),
                ],
              )
            : CupertinoAlertDialog(
                title: Text('Do you want to exit application?'),
                actions: [
                  TextButton(
                      onPressed: () => Navigator.pop(context, false),
                      child: Text(
                        'NO',
                      )),
                  TextButton(
                      onPressed: () => Navigator.pop(context, true),
                      child: Text(
                        'YES',
                      )),
                ],
              ));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => confirmExit(context),
      child: Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(
                  SizeUtil().calculateHeightPx(context, 24),
                  SizeUtil().calculateHeightPx(context, 16),
                  SizeUtil().calculateWidthPx(context, 16),
                  SizeUtil().calculateWidthPx(context, 24),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'To-do list',
                      style: TextStyle(
                        fontSize: SizeUtil().calculateHeightPx(context, 24),
                        fontWeight: FontWeight.w500,
                        height: SizeUtil().calculateHeightPx(context, 1.33),
                        letterSpacing: SizeUtil().calculateWidthPx(context, -0.22),
                        color: defaultTextColor,
                      ),
                    ),
                    GestureDetector(
                      onTap: () => addTodo(context),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(SizeUtil().calculateHeightPx(context, 20)),
                          color: Color.fromRGBO(1, 176, 102, 1),
                        ),
                        height: SizeUtil().calculateHeightPx(context, 40),
                        width: SizeUtil().calculateHeightPx(context, 60),
                        child: Center(
                          child: Icon(
                            Icons.add,
                            size: SizeUtil().calculateHeightPx(context, 24),
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: ListTileTheme(
                    contentPadding: EdgeInsets.only(left: SizeUtil().calculateWidthPx(context, 24), right: SizeUtil().calculateWidthPx(context, 16)),
                    iconColor: defaultTextColor,
                    child: ExpansionTile(
                        initiallyExpanded: true,
                        childrenPadding: EdgeInsets.only(left: SizeUtil().calculateWidthPx(context, 24)),
                        title: Text(
                          'All',
                          style: TextStyle(
                            fontSize: SizeUtil().calculateHeightPx(context, 16),
                            color: Color.fromRGBO(69, 76, 96, 2),
                          ),
                        ),
                        children: context.watch<TodoList>().getTodoList.map((e) => _buildTodoListTile(context, e)).toList()),
                  ),
                ),
              ),
              SizedBox(
                height: SizeUtil().calculateHeightPx(context, 50),
                width: double.infinity,
                child: TextButton(
                    style: ButtonStyle(
                      foregroundColor: MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
                    ),
                    onPressed: () => TodoController().logout(context),
                    child: Text(
                      'Log out',
                      style: TextStyle(fontSize: SizeUtil().calculateHeightPx(context, 16)),
                    )),
              ),
              SizedBox(height: SizeUtil().calculateHeightPx(context, 35))
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTodoListTile(BuildContext context, TodoModel todo) => GestureDetector(
        onTap: () => TodoController().updateTodoContent(context, todo),
        child: Container(
          height: SizeUtil().calculateHeightPx(context, 60),
          width: double.infinity,
          decoration: BoxDecoration(
              border: Border(
                  bottom: context.read<TodoList>().getTodoList.last.content == todo.content
                      ? BorderSide.none
                      : BorderSide(color: Color.fromRGBO(221, 221, 221, 1)))),
          child: Row(
            children: [
              Container(
                height: SizeUtil().calculateHeightPx(context, 24),
                width: SizeUtil().calculateWidthPx(context, 24),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: todo.isDone == true ? Colors.green : Colors.white,
                  border: Border.all(color: greyColor),
                ),
                child: todo.isDone
                    ? Center(
                        child: Icon(
                        Icons.check,
                        color: Colors.white,
                        size: SizeUtil().calculateHeightPx(context, 15),
                      ))
                    : null,
              ),
              SizedBox(width: SizeUtil().calculateWidthPx(context, 16)),
              Text(
                '${todo.content}',
                style: TextStyle(
                    fontSize: SizeUtil().calculateHeightPx(context, 16), decoration: todo.isDone ? TextDecoration.lineThrough : TextDecoration.none),
              ),
            ],
          ),
        ),
      );
}
