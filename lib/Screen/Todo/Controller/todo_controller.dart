import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/Model/todo_model.dart';
import 'package:todo_app/Provider/todo_list.dart';

class TodoController {
  void logout(BuildContext context) => Navigator.pushReplacementNamed(context, '/Login');

  addTodoContent(BuildContext context, String content, String dueDate) async {
    var todoListProvider = context.read<TodoList>();
    todoListProvider.setTodoList([TodoModel(content, dueDate, DateTime.now().toIso8601String(), false), ...todoListProvider.getTodoList]);
  }

  updateTodoContent(BuildContext context, TodoModel todo) {
    var todoListProvider = context.read<TodoList>();
    List<TodoModel> todoList = todoListProvider.getTodoList;
    int index = todoList.indexWhere((element) => element.createTime == todo.createTime);
    todo.updateIsDone();
    todoList[index] = todo;
    todoListProvider.setTodoList(todoList);
  }
}
