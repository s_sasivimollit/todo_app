import 'package:flutter/material.dart';
import 'package:todo_app/Screen/Login/Controller/login_controller.dart';
import 'package:todo_app/Util/constants.dart';
import 'package:todo_app/Util/util.dart';

class Login extends StatelessWidget {
  final _email = TextEditingController(text: 'email@todoapp.com');
  final _password = TextEditingController(text: 'my_password');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeUtil().calculateHeightPx(context, 40)),
        child: Column(
          children: [
            Expanded(child: Container()),
            Expanded(
              flex: 4,
              child: Column(
                children: [
                  Text(
                    'Login',
                    style: TextStyle(
                      fontSize: SizeUtil().calculateHeightPx(context, 32),
                      height: 1.33,
                      letterSpacing: SizeUtil().calculateWidthPx(context, -0.29),
                      fontWeight: FontWeight.w500,
                      color: defaultTextColor,
                    ),
                  ),
                  SizedBox(height: SizeUtil().calculateHeightPx(context, 48)),
                  SizedBox(
                    height: SizeUtil().calculateHeightPx(context, 48),
                    child: TextField(
                      controller: _email,
                      keyboardType: TextInputType.emailAddress,
                      style: TextStyle(fontSize: SizeUtil().calculateHeightPx(context, 16)),
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(SizeUtil().calculateHeightPx(context, 6.0)),
                              borderSide: BorderSide(color: greyColor)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(SizeUtil().calculateHeightPx(context, 6.0)),
                              borderSide: BorderSide(color: greyColor)),
                          contentPadding: EdgeInsets.only(left: SizeUtil().calculateWidthPx(context, 12)),
                          hintText: 'Email',
                          hintStyle: TextStyle(fontSize: SizeUtil().calculateHeightPx(context, 16), color: greyColor)),
                    ),
                  ),
                  SizedBox(height: SizeUtil().calculateHeightPx(context, 8)),
                  SizedBox(
                    height: SizeUtil().calculateHeightPx(context, 48),
                    child: TextField(
                      controller: _password,
                      keyboardType: TextInputType.emailAddress,
                      style: TextStyle(fontSize: SizeUtil().calculateHeightPx(context, 16)),
                      obscureText: true,
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(SizeUtil().calculateHeightPx(context, 6.0)),
                              borderSide: BorderSide(color: greyColor)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(SizeUtil().calculateHeightPx(context, 6.0)),
                              borderSide: BorderSide(color: greyColor)),
                          contentPadding: EdgeInsets.only(left: SizeUtil().calculateWidthPx(context, 12)),
                          hintText: 'Password',
                          hintStyle: TextStyle(fontSize: SizeUtil().calculateHeightPx(context, 16), color: greyColor)),
                    ),
                  ),
                  SizedBox(height: SizeUtil().calculateHeightPx(context, 24)),
                  SizedBox(
                    height: SizeUtil().calculateHeightPx(context, 48),
                    child: TextButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Color.fromRGBO(1, 178, 102, 1)),
                          shape: MaterialStateProperty.all<OutlinedBorder>(
                              RoundedRectangleBorder(borderRadius: BorderRadius.circular(SizeUtil().calculateHeightPx(context, 6))))),
                      onPressed: () => LoginController().login(context, email: _email.text, password: _password.text),
                      child: Center(
                        child: Text(
                          'Login',
                          style: TextStyle(
                            fontSize: SizeUtil().calculateHeightPx(context, 16),
                            fontWeight: FontWeight.normal,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
