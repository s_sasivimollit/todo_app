import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginController {
  login(BuildContext context, {String email, String password}) {
    if (email.isNotEmpty && password.isNotEmpty && email == 'email@todoapp.com' && password == 'my_password')
      Navigator.pushReplacementNamed(context, '/Todo');
    else
      showDialog(
        context: context,
        builder: (context) => Platform.isAndroid
            ? AlertDialog(
                title: Text('Login failed'),
                content: Text('Invalid username or password'),
                actions: [FlatButton(onPressed: () => Navigator.pop(context), child: Text('OK'))],
              )
            : CupertinoAlertDialog(
                title: Text('Login failed'),
                content: Text('Invalid username or password'),
                actions: [FlatButton(onPressed: () => Navigator.pop(context), child: Text('OK'))],
              ),
      );
  }
}
