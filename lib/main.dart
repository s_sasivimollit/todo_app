import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/Provider/todo_list.dart';
import 'package:todo_app/Screen/Login/UI/login.dart';
import 'package:todo_app/Screen/Todo/UI/todo.dart';
import 'package:todo_app/Util/constants.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => TodoList()),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo App',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        fontFamily: 'Mitr',
        textTheme: Theme.of(context)
            .textTheme
            .apply(
              fontFamily: 'Mitr',
              bodyColor: defaultTextColor,
            )
            .copyWith(
              button: TextStyle(height: 1.33),
              bodyText2: TextStyle(height: 1.33),
            ),
        primaryColor: defaultTextColor,
      ),
      initialRoute: '/Login',
      routes: {
        '/Login': (context) => Login(),
        '/Todo': (context) => Todo(),
      },
    );
  }
}
