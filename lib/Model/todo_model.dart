class TodoModel {
  TodoModel(this._content, this._dueDate, this._createTime, this._isDone);
  String _content;
  String _dueDate;
  String _createTime;
  bool _isDone;

  bool get isDone => _isDone;
  String get content => _content;
  String get dueDate => _dueDate;
  String get createTime => _createTime;

  void updateIsDone() {
    _isDone = !_isDone;
  }
}
