import 'package:flutter/material.dart';
import 'package:todo_app/Model/todo_model.dart';

class TodoList with ChangeNotifier {
  List<TodoModel> _todoList = [];

  List<TodoModel> get getTodoList {
    // if (_todoList.length > 1) _todoList.sort((a, b) => DateTime.parse(b.createTime).compareTo(DateTime.parse(a.createTime)));
    return _todoList;
  }

  void setTodoList(List<TodoModel> list) {
    _todoList = list;
    notifyListeners();
  }
}
