import 'package:flutter/material.dart';

class SizeUtil {
  double calculateWidthPx(BuildContext context, double pt) =>
      (((pt / 414) * (MediaQuery.of(context).size.width * MediaQuery.of(context).devicePixelRatio)).floorToDouble()) /
      MediaQuery.of(context).devicePixelRatio;

  double calculateHeightPx(BuildContext context, double pt) =>
      (((pt / 896) * (MediaQuery.of(context).size.height * MediaQuery.of(context).devicePixelRatio)).floorToDouble()) /
      MediaQuery.of(context).devicePixelRatio;

  double calculateRadius(BuildContext context, double pt) => (pt * 1.33).floorToDouble() / MediaQuery.of(context).devicePixelRatio;
}

class DateUtil {
  String convertToBE(String isoDate) {
    List dateName = [
      'มกราคม',
      'กุมภาพันธ์',
      'มีนาคม',
      'เมษายน',
      'พฤษภาคม',
      'มิถุนายน',
      'กรกฎาคม',
      'สิงหาคม',
      'กันยายน',
      'ตุลาคม',
      'พฤศจิกายน',
      'ธันวาคม',
    ];
    DateTime dateTime = DateTime.parse(isoDate);
    return '${dateTime.day} / ${dateName[dateTime.month - 1]} / ${dateTime.year + 543}';
  }
}
